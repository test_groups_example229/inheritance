/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.inheritance;

/**
 *
 * @author Tuf Gaming
 */
public class Duck2 extends Animal{
    private int numberOfWings;
    public Duck2(String name, String color){
        super(name, color, 2);
        System.out.println("----------------------------------");
        System.out.println("Duck2 created");
       
       }
    @Override
        public void speak() {
         super.speak(); 
         System.out.println("Duck:" + name + " speak > Kab Kab!!");
   
    }

    @Override
 public void walk() {
       super.walk(); 

        System.out.println("Duck:" + name + " walk with " + numberOfLegs + " legs.");
       
    }

    public void fly(){
        System.out.println("Duck: " + name + " fly!!!");
        
         System.out.println("----------------------------------");
    }
}

