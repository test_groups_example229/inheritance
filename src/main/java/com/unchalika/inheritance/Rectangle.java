/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.inheritance;

/**
 *
 * @author Tuf Gaming
 */
public class Rectangle extends Shape{
    private double w,h;
    public Rectangle(double w,double h){
        System.out.println("Rectangle created");
        this.w=w;
        this.h=h;
    }
    @Override
    public double CalArea (){
        return w*h;
    }
}