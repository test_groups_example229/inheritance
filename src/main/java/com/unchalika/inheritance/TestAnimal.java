/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.inheritance;

/**
 *
 * @author Tuf Gaming
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();

        Dog dang = new Dog("Dang", "Black&White");
        dang.speak();
        dang.ncl();
        dang.walk();
        
        Dog2 mome = new Dog2("Mome", "Black&White");
        mome.speak();
        mome.ncl();
        mome.walk();
        
        Dog3 bat = new Dog3("Bat", "Black&White");
        bat.speak();
        bat.ncl();
        bat.walk();

        Dog4 to = new Dog4("To", "Black&White");
        to.speak();
        to.ncl();
        to.walk();

        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.ncl();
        zero.walk();

        Duck som = new Duck("Som", "Orange");
        som.speak();
        som.ncl();
        som.walk();
        som.fly();
        
        Duck2 gabgab = new Duck2("GabGab", "Black");
        gabgab.speak();
        gabgab.ncl();
        gabgab.walk();
        gabgab.fly();

        System.out.println("Som is Animal: " + (som instanceof Animal));
        System.out.println("Som is Duck: " + (som instanceof Duck));
        System.out.println("Som is Cat: " + (som instanceof Object));
        System.out.println("Som is Dog: " + (animal instanceof Dog));
        System.out.println("Animal is Animal: " + (animal instanceof Animal));
        
        
        Animal[] animals = {dang, mome, bat, to, zero, som, gabgab};
        for (int i = 0; i < animals.length; i++) {
            animals[i].walk();
            animals[i].speak();
            if (animals[i] instanceof Duck) {
                Duck duck = (Duck) animals[i];
                duck.fly();
            }
        }

    }
}
